﻿using Krzyzyk.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Krzyzyk.Repositoris
{

  class PlayerRecordRepo
  {
	 DbContext DbContext = null;

	 public PlayerRecordRepo()
	 {
		DbContext = new DbContext();
	 }

	 public List<PlayerRecord> GetPlayerRecordsList()
	 {
		return DbContext.PlayerRecord.OrderBy(i => i.Time).Take(5).ToList();
	 }
  }
}
