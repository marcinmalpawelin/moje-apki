﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Krzyzyk.Models
{
	class Player
	{
		public string Playername { get; set; }
		public char Charakter { get; set; }

		public void SetName(string Name)
		{
			Playername = Name;
		}
	}
}
