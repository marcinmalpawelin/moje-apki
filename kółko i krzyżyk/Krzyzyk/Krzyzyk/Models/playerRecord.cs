﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Krzyzyk.Models
{
  public class PlayerRecord
  {
	 public Guid Id { get; set; }
	 public string Name { get; set; }
	 public DateTime Time { get; set; }
	 public DateTime Date { get; set; }		
  }
}
