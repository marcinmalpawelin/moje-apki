﻿namespace Krzyzyk
{
  partial class Start
  {
	 /// <summary>
	 /// Wymagana zmienna projektanta.
	 /// </summary>
	 private System.ComponentModel.IContainer components = null;

	 /// <summary>
	 /// Wyczyść wszystkie używane zasoby.
	 /// </summary>
	 /// <param name="disposing">prawda, jeżeli zarządzane zasoby powinny zostać zlikwidowane; Fałsz w przeciwnym wypadku.</param>
	 protected override void Dispose(bool disposing)
	 {
		if(disposing && (components != null))
		{
		  components.Dispose();
		}
		base.Dispose(disposing);
	 }

	 #region Kod generowany przez Projektanta formularzy systemu Windows

	 /// <summary>
	 /// Metoda wymagana do obsługi projektanta — nie należy modyfikować
	 /// jej zawartości w edytorze kodu.
	 /// </summary>
	 private void InitializeComponent()
	 {
			this.button1 = new System.Windows.Forms.Button();
			this.button2 = new System.Windows.Forms.Button();
			this.button3 = new System.Windows.Forms.Button();
			this.SuspendLayout();
			// 
			// button1
			// 
			this.button1.BackColor = System.Drawing.Color.YellowGreen;
			this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
			this.button1.Location = new System.Drawing.Point(276, 81);
			this.button1.Name = "button1";
			this.button1.Size = new System.Drawing.Size(257, 31);
			this.button1.TabIndex = 0;
			this.button1.Text = "Graj";
			this.button1.UseVisualStyleBackColor = false;
			this.button1.Click += new System.EventHandler(this.button1_Click);
			// 
			// button2
			// 
			this.button2.BackColor = System.Drawing.Color.YellowGreen;
			this.button2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
			this.button2.Location = new System.Drawing.Point(276, 118);
			this.button2.Name = "button2";
			this.button2.Size = new System.Drawing.Size(257, 31);
			this.button2.TabIndex = 1;
			this.button2.Text = "Wyniki";
			this.button2.UseVisualStyleBackColor = false;
			// 
			// button3
			// 
			this.button3.BackColor = System.Drawing.Color.YellowGreen;
			this.button3.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
			this.button3.Location = new System.Drawing.Point(276, 155);
			this.button3.Name = "button3";
			this.button3.Size = new System.Drawing.Size(257, 31);
			this.button3.TabIndex = 2;
			this.button3.Text = "Wyjdz";
			this.button3.UseVisualStyleBackColor = false;
			// 
			// Start
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(800, 450);
			this.Controls.Add(this.button3);
			this.Controls.Add(this.button2);
			this.Controls.Add(this.button1);
			this.Name = "Start";
			this.Text = "Form1";
			this.ResumeLayout(false);

	 }

		#endregion

		private System.Windows.Forms.Button button1;
		private System.Windows.Forms.Button button2;
		private System.Windows.Forms.Button button3;
	}
}

