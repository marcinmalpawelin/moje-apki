﻿namespace Krzyzyk.Views
{
	partial class GameSetting
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if(disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.label1 = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this._NazwaGracza1 = new System.Windows.Forms.TextBox();
			this._NazwaGracza2 = new System.Windows.Forms.TextBox();
			this.label3 = new System.Windows.Forms.Label();
			this.label4 = new System.Windows.Forms.Label();
			this._LosowanieStron = new System.Windows.Forms.CheckBox();
			this._gracz1Krzyzyk = new System.Windows.Forms.CheckBox();
			this._gracz2Krzyzyk = new System.Windows.Forms.CheckBox();
			this.button1 = new System.Windows.Forms.Button();
			this.SuspendLayout();
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
			this.label1.Location = new System.Drawing.Point(112, 9);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(109, 31);
			this.label1.TabIndex = 0;
			this.label1.Text = "Gracz 1";
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
			this.label2.Location = new System.Drawing.Point(541, 9);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(109, 31);
			this.label2.TabIndex = 1;
			this.label2.Text = "Gracz 2";
			// 
			// _NazwaGracza1
			// 
			this._NazwaGracza1.Location = new System.Drawing.Point(12, 98);
			this._NazwaGracza1.Name = "_NazwaGracza1";
			this._NazwaGracza1.Size = new System.Drawing.Size(316, 20);
			this._NazwaGracza1.TabIndex = 2;
			this._NazwaGracza1.TextChanged += new System.EventHandler(this.Player1NameChanged);
			// 
			// _NazwaGracza2
			// 
			this._NazwaGracza2.Location = new System.Drawing.Point(472, 98);
			this._NazwaGracza2.Name = "_NazwaGracza2";
			this._NazwaGracza2.Size = new System.Drawing.Size(316, 20);
			this._NazwaGracza2.TabIndex = 3;
			this._NazwaGracza2.TextChanged += new System.EventHandler(this.Player2NameChanged);
			// 
			// label3
			// 
			this.label3.AutoSize = true;
			this.label3.Location = new System.Drawing.Point(146, 82);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(75, 13);
			this.label3.TabIndex = 4;
			this.label3.Text = "Nazwa gracza";
			// 
			// label4
			// 
			this.label4.AutoSize = true;
			this.label4.Location = new System.Drawing.Point(575, 82);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(75, 13);
			this.label4.TabIndex = 5;
			this.label4.Text = "Nazwa gracza";
			// 
			// _LosowanieStron
			// 
			this._LosowanieStron.AutoSize = true;
			this._LosowanieStron.Location = new System.Drawing.Point(341, 101);
			this._LosowanieStron.Name = "_LosowanieStron";
			this._LosowanieStron.Size = new System.Drawing.Size(125, 17);
			this._LosowanieStron.TabIndex = 6;
			this._LosowanieStron.Text = "Czy losować strony ?";
			this._LosowanieStron.UseVisualStyleBackColor = true;
			// 
			// _gracz1Krzyzyk
			// 
			this._gracz1Krzyzyk.AutoSize = true;
			this._gracz1Krzyzyk.Location = new System.Drawing.Point(149, 124);
			this._gracz1Krzyzyk.Name = "_gracz1Krzyzyk";
			this._gracz1Krzyzyk.Size = new System.Drawing.Size(33, 17);
			this._gracz1Krzyzyk.TabIndex = 7;
			this._gracz1Krzyzyk.Text = "X";
			this._gracz1Krzyzyk.UseVisualStyleBackColor = true;
			// 
			// _gracz2Krzyzyk
			// 
			this._gracz2Krzyzyk.AutoSize = true;
			this._gracz2Krzyzyk.Location = new System.Drawing.Point(578, 124);
			this._gracz2Krzyzyk.Name = "_gracz2Krzyzyk";
			this._gracz2Krzyzyk.Size = new System.Drawing.Size(33, 17);
			this._gracz2Krzyzyk.TabIndex = 8;
			this._gracz2Krzyzyk.Text = "X";
			this._gracz2Krzyzyk.UseVisualStyleBackColor = true;
			// 
			// button1
			// 
			this.button1.BackColor = System.Drawing.Color.YellowGreen;
			this.button1.Location = new System.Drawing.Point(341, 248);
			this.button1.Name = "button1";
			this.button1.Size = new System.Drawing.Size(125, 32);
			this.button1.TabIndex = 9;
			this.button1.Text = "Graj!!";
			this.button1.UseVisualStyleBackColor = false;
			// 
			// GameSetting
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(800, 450);
			this.Controls.Add(this.button1);
			this.Controls.Add(this._gracz2Krzyzyk);
			this.Controls.Add(this._gracz1Krzyzyk);
			this.Controls.Add(this._LosowanieStron);
			this.Controls.Add(this.label4);
			this.Controls.Add(this.label3);
			this.Controls.Add(this._NazwaGracza2);
			this.Controls.Add(this._NazwaGracza1);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.label1);
			this.Name = "GameSetting";
			this.Text = "GameSetting";
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion
		private System.Windows.Forms.CheckedListBox checkedListBox1;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.TextBox _NazwaGracza1;
		private System.Windows.Forms.TextBox _NazwaGracza2;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.CheckBox _LosowanieStron;
		private System.Windows.Forms.CheckBox _gracz1Krzyzyk;
		private System.Windows.Forms.CheckBox _gracz2Krzyzyk;
		private System.Windows.Forms.Button button1;
	}
}