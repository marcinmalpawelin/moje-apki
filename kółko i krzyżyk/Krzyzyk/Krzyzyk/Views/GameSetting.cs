﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Krzyzyk.Views
{
	public partial class GameSetting : Form
	{
		public GameSetting()
		{
			InitializeComponent();
		}

		private void Player1NameChanged(object sender, EventArgs e)
		{
			label1.Text = _NazwaGracza1.Text;

		}

		private void Player2NameChanged(object sender, EventArgs e)
		{
			label2.Text = _NazwaGracza2.Text;

		}
	}
}
